<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;

class Reg extends Component
{
    public $name;
    public $email;
    public $password;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'min:4',
            'email' => 'email|unique:users',
            'password' => 'min:6'
        ],[
            'name.required' => 'Необходимо указать имя',
            'name.min' => 'Длина имени должна быть не менее :min символов',
            'email.required' => 'Необходимо указать Email',
            'email.email' => 'Неверный формат Email',
            'password.required' => 'Необходимо указать пароль',
            'password.min' => 'Длина пароля должна быть не менее :min символов',
        ]);
    }

    public function submit()
    {
        $this->validate([
            'name' => 'required|min:4|string',
            'email' => 'required|email|string|unique:users',
            'password' => 'required|min:6|string'
        ],[
            'name.required' => 'Необходимо указать имя',
            'name.min' => 'Длина имени должна быть не менее :min символов',
            'email.required' => 'Необходимо указать Email',
            'email.email' => 'Неверный формат Email',
            'password.required' => 'Необходимо указать пароль',
            'password.min' => 'Длина пароля должна быть не менее :min символов',
        ]);

        // Execution doesn't reach here if validation fails.

        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this -> name)
        ]);
        Auth::guard() -> login($user);
        return redirect()->route('reg-success');
    }
    public function render()
    {
        return view('livewire.reg');
    }
}
