<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Enter extends Component
{
    public $email;
    public $password;
    public $auth = true;

    public function submit()
    {
        $this->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ],[
            'email.required' => 'Необходимо указать Email',
            'password.required' => 'Необходимо указать пароль',
        ]);
        if (Auth::attempt(['email' => $this -> email, 'password' => $this -> password])) {
            // Аутентификация успешна
            return redirect()->route('home');
        } else $this -> auth = false;
    }
    public function render()
    {
        return view('livewire.enter');
    }
}
