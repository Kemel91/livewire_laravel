<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg() {
        return view('auth.reg');
    }

    public function enter() {
        return view('auth.enter');
    }
}
