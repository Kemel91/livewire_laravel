<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            <a class="navbar-brand" href="#">SP<i class="fa fa-circle"></i>T</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
            <li class="{{ Route::currentRouteName() == 'home' ? 'active' : ''}}"><a href="{{ route('home') }}">Главная</a></li>
              <li class=""><a href="about.html">ABOUT</a></li>
              <li class="{{ Route::currentRouteName() == 'enter' ? 'active' : ''}}"><a href="{{ route('enter')}}">Вход</a></li>
              <li class="{{ Route::currentRouteName() == 'reg' ? 'active' : ''}}"><a href="{{ route('reg') }}">Регистрация</a></li>
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
      </div>
        <main class="py-4">
            @yield('content')
        </main>

        <div id="copyrights">
                <div class="container">
                  <p>
                    &copy; Copyrights <strong>Kemel</strong>. Все права защищены
                  </p>
                  <div class="credits">
                    Информация дополнительная
                  </div>
                </div>
        </div>
    </div>
    @livewireAssets
</body>
</html>
