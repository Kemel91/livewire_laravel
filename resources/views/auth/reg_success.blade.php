@extends('layouts.app')

@section('content')
<div class="container desc">
    <div class="row">
    <div class="col-lg-6 centered">
        <img src="/images/p03.png" alt />
    </div>
    <div class="col-lg-6">
        <div class="alert alert-success" role="alert">
            Регистрация прошла успешно!
        </div>
    </div>
    </div>
</div>
@endsection