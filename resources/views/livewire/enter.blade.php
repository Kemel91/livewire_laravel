<div>
        <div wire:loading wire:target="submit">
            Проверка...
        </div>
    
        @if ($this -> auth === false)
            <div class="alert alert-danger" role="alert">
                Неверный логин или пароль
            </div>
        @endif
        <form wire:submit.prevent="submit">
            <div class="form-group">
                <label for="email">Email</label>
                <input wire:model="email" class="form-control" type="email" name="email" placeholder="Введите email"
                    aria-label="Recipient's ">
                <div class="input-group-append">
                    @error('email')<span class="input-group-text">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <input wire:model="password" class="form-control" type="password" name="password" placeholder="Введите пароль"
                    aria-label="Recipient's ">
                <div class="input-group-append">
                    @error('password')<span class="input-group-text">{{ $message }}</span>@enderror
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Войти</button>
        </form>
    </div>
