<div>
        <div wire:loading wire:target="submit">
            Проверка...
        </div>
    
        <form wire:submit.prevent="submit">
            <div class="form-group">
                <label for="email">Email</label>
                <input wire:model="email" class="form-control" type="email" name="email" placeholder="Введите email"
                    aria-label="Recipient's ">
                <div class="input-group-append">
                    @error('email')<span class="input-group-text">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="form-group">
                <label for="name">Имя</label>
                <input wire:model="name" class="form-control" type="text" name="name" placeholder="Введите имя"
                    aria-label="Recipient's ">
                <div class="input-group-append">
                    @error('name')<span class="input-group-text">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <input wire:model="password" class="form-control" type="password" name="password" placeholder="Введите пароль"
                    aria-label="Recipient's ">
                <div class="input-group-append">
                    @error('password')<span class="input-group-text">{{ $message }}</span>@enderror
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Регистрация</button>
        </form>
    </div>