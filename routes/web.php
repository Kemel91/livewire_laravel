<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Symfony\Component\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/enter', 'AuthController@enter')->name('enter');
Route::get('/reg', 'AuthController@reg')->name('reg');
Route::get('/reg-success', function () {
    return view('auth.reg_success');
}) -> middleware('auth') ->name('reg-success');
Route::get('/home', 'HomeController@index') -> middleware('auth') ->name('home');
